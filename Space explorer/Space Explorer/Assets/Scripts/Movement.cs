using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Movement : MonoBehaviour
{
    public float movementSpeed = 6f;
    public float rotationSpeed = 20f;
    public float boostMultiplier = 2f;
    public float boostDuration = 1f;

    private bool isBoostActive = false;

    public Sprite explosionSprite;

    private void Start()
    {
        GetComponent<SpriteRenderer>().sprite = explosionSprite;
    }

    void Update()
    {
        Move();
        Rotate();
        if (Input.GetKeyDown(KeyCode.Space) && !isBoostActive)
        {
            StartCoroutine(ActivateBoost());
        }
        LimitPosition();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameOver();
    }

    void GameOver()
    {

        StartCoroutine(Wait());
    }

    IEnumerator Wait()
    {
        GetComponent<SpriteRenderer>().sprite = explosionSprite;

        yield return new WaitForSeconds(0f);

        SceneManager.LoadScene("GameOverScene");
    }

    void Move()
    {
        float verticalInput = Input.GetAxis("Vertical");
        float currentSpeed = isBoostActive ? movementSpeed * boostMultiplier : movementSpeed;
        transform.Translate(Vector3.down * verticalInput * currentSpeed * Time.deltaTime);
    }

    void Rotate()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        transform.Rotate(Vector3.back * horizontalInput * rotationSpeed * Time.deltaTime);
    }

    void LimitPosition()
    {
        Camera mainCamera = Camera.main;
        float screenHalfWidth = mainCamera.orthographicSize * Screen.width / Screen.height;

        float xPosition = Mathf.Clamp(transform.position.x, -screenHalfWidth, screenHalfWidth);
        float yPosition = Mathf.Clamp(transform.position.y, -mainCamera.orthographicSize, mainCamera.orthographicSize);

        transform.position = new Vector3(xPosition, yPosition, transform.position.z);
    }

    System.Collections.IEnumerator ActivateBoost()
    {
        isBoostActive = true;
        movementSpeed *= boostMultiplier;

        yield return new WaitForSeconds(boostDuration);

        movementSpeed /= boostMultiplier;
        isBoostActive = false;
    }
}
