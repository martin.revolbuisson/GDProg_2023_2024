using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collisions : MonoBehaviour
{
    public Sprite explosionSprite;

    private float limiteCamera = -10f;
    private bool isExploding = false;

    private void Update()
    {
        if (!isExploding && transform.position.y < limiteCamera)
        {
            StartExplosion();
        }
        LimitPosition();
    }

    void StartExplosion()
    {
        isExploding = true;

        if (explosionSprite != null)
        {
            GetComponent<SpriteRenderer>().sprite = explosionSprite;
        }

        StartCoroutine(DestroyAfterDelay(0.5f));
    }
    void LimitPosition()
    {
        Camera mainCamera = Camera.main;
        float screenHalfWidth = mainCamera.orthographicSize * Screen.width / Screen.height;

        float xPosition = Mathf.Clamp(transform.position.x, -screenHalfWidth, screenHalfWidth);
        //float yPosition = Mathf.Clamp(transform.position.y, -mainCamera.orthographicSize, mainCamera.orthographicSize);

        transform.position = new Vector3(xPosition, transform.position.y, transform.position.z);
    }


    IEnumerator DestroyAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);

        Destroy(gameObject);
    }

}
