using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnMeteorites : MonoBehaviour
{
    public GameObject meteoritePrefab;
    public float spawnRate = 2f;

    private float nextSpawnTime = 0f;

    private void Update()
    {
        if (Time.time >= nextSpawnTime) 
        {
            SpawnMeteorite();
            nextSpawnTime = Time.time + 1f / spawnRate;
        }
    }

    void SpawnMeteorite()
    {
        float randomX = Random.Range(-20f, 20f);
        Vector2 spawnPosition = new Vector2(randomX, 15f);

        Instantiate(meteoritePrefab, spawnPosition, Quaternion.identity);
    }
}
