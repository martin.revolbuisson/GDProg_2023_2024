using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TimeToWin : MonoBehaviour
{
    public int Time = 30;

    public TMP_Text restTime;

    public GameObject winMenuUI;

    void Start()
    {
        StartCoroutine(EveryTime());
    }

    void Update()
    {
        restTime.text = $"Time = {Time}";
        if (Time < 1)
        {
            WinPage();
        }
    }

    void WinPage()
    {
        if (winMenuUI != null) { winMenuUI.SetActive(true); }
        Time.timeScale = 0f;
    }

    IEnumerator EveryTime()
    {
        while (true)
        {
            Time--;

            yield return new WaitForSeconds(1f) ;
        }
    }
}
