# Space Explorer - 2D Space Adventure Game

## Introduction:

Embark on an interstellar journey with Space Explorer, a solo 2D space adventure game. Pilot your spaceship through asteroid fields.

## How to Launch the Game:

1. **Clone the Project:** Use the following command in your terminal to clone the project.
```bash
git clone https://gitlab.com/martin.revolbuisson/GDProg_2023_2024/-/tree/main/Space%20explorer?ref_type=heads
```
2. **Open the Project in Unity:** Launch Unity and open the project by selecting the cloned folder.
3. **Start the Game:** Navigate to the main scene and press the "Play" button to begin playing.