# Belowcooked - 2D Cooking Game

## Introduction:
Belowcooked is a solo 2D cooking game where you have 3 minutes to prepare and serve as many soups as possible. Take on the time challenge and ensure each bowl is a delightful culinary masterpiece.

## How to Launch the Game:

1. **Clone the Project:** Use the following command in your terminal to clone the project.
```bash
git clone https://gitlab.com/martin.revolbuisson/GDProg_2023_2024/-/tree/main/Project%20Overcooked%202d?ref_type=heads
```
2. **Open the Project in Unity:** Launch Unity and open the project by selecting the cloned folder.
3. **Start the Game:** Navigate to the main scene and press the "Play" button to begin playing.