using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RamassageObjet : MonoBehaviour
{
    public float rayonDetection = 0.5f;
    public LayerMask objetRamassableLayer;

    private GameObject objetRamasse;
    private Vector2 dernierMouvement = Vector2.right; // Initialise avec la droite par d�faut
    
    
    public GameObject Tomato;
    public GameObject Champi;
    public GameObject Carot;


    public GameObject SlicedTomato;
    public GameObject SlicedChampi;
    public GameObject SlicedCarot;


    public GameObject TomatoFour;
    public GameObject ChampiFour;
    public GameObject CarotFour;
    public GameObject Four;


    public bool haveChampi = false;
    public bool haveSlicedChampi = false;
    public bool haveChampiSoup = false;

    public bool haveCarot = false;
    public bool haveSlicedCarot = false;
    public bool haveCarotSoup = false;

    public bool haveTomato = false;
    public bool haveSlicedTomato = false;
    public bool haveTomatoSoup = false;

    public bool isSelled = false;

    public int counterIngredient = 0;

    public List<GameObject> GoodSoup = new List<GameObject>();

    public Text inventory;


    void Update()
    {
        //print(haveSlicedTomato);
        //print(haveTomato);
        DetecterDernierMouvement();

        if (counterIngredient > 3)
        {
            //print("AAAAAAAAAAAAAAHHHHHHHHHH");
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            Collider2D nearbyObject = Physics2D.OverlapCircle(transform.position + (Vector3)dernierMouvement * rayonDetection, rayonDetection, objetRamassableLayer);

            if (haveTomato)
            {
                //inventory.text = "Inventory : Tomato";
                if (nearbyObject)
                {
                    if (nearbyObject.CompareTag("Planche"))
                    {
                        haveTomato = false;
                        haveSlicedTomato = true;
                    }
                }
                 else
                {
                    haveTomato = false;
                    Vector2 spawnPosition = new Vector2(transform.position.x + dernierMouvement.x * (rayonDetection * -1), transform.position.y + dernierMouvement.y * (rayonDetection * -1));
                    Instantiate(Tomato, spawnPosition, Quaternion.identity);
                }
            }
            else if (haveChampi)
            {
                //inventory.text = "Inventory : Mushroom";
                if (nearbyObject)
                {
                    if (nearbyObject.CompareTag("Planche"))
                    {
                        haveChampi = false;
                        haveSlicedChampi = true;
                    }
                }
                else
                {
                    haveChampi = false;
                    Vector2 spawnPosition = new Vector2(transform.position.x + dernierMouvement.x * (rayonDetection * -1), transform.position.y + dernierMouvement.y * (rayonDetection * -1));
                    Instantiate(Champi, spawnPosition, Quaternion.identity);
                }
            }
            else if (haveCarot)
            {
                //inventory.text = "Inventory : Carot";
                if (nearbyObject)
                {
                    if (nearbyObject.CompareTag("Planche"))
                    {
                        haveCarot = false;
                        haveSlicedCarot = true;
                    }
                }
                else
                {
                    haveCarot = false;
                    Vector2 spawnPosition = new Vector2(transform.position.x + dernierMouvement.x * (rayonDetection * -1), transform.position.y + dernierMouvement.y * (rayonDetection * -1));
                    Instantiate(Carot, spawnPosition, Quaternion.identity);
                }
            }
            else if (haveSlicedTomato)
            {
                //inventory.text = "Inventory : SlicedTomato";
                if (nearbyObject)
                {
                    if (nearbyObject.CompareTag("Recipient"))
                    {
                        haveSlicedTomato = false;
                        GoodSoup.Add(Tomato);
                        counterIngredient++;
                    }
                }
                else
                {
                    haveSlicedTomato = false;
                    Vector2 spawnPosition = new Vector2(transform.position.x + dernierMouvement.x * (rayonDetection * -1), transform.position.y + dernierMouvement.y * (rayonDetection * -1));
                    Instantiate(SlicedTomato, spawnPosition, Quaternion.identity);
                }
            }
            else if (haveSlicedChampi)
            {
                //inventory.text = "Inventory : SlicedMushroom";
                if (nearbyObject)
                {
                    if (nearbyObject.CompareTag("Recipient"))
                    {
                        haveSlicedChampi = false;
                        GoodSoup.Add(Champi);
                        counterIngredient++;
                    }
                }
                else
                {
                    haveSlicedChampi = false;
                    Vector2 spawnPosition = new Vector2(transform.position.x + dernierMouvement.x * (rayonDetection * -1), transform.position.y + dernierMouvement.y * (rayonDetection * -1));
                    Instantiate(SlicedChampi, spawnPosition, Quaternion.identity);
                }
            }
            else if (haveSlicedCarot)
            {
                //inventory.text = "Inventory : SlicedCarot";
                if (nearbyObject)
                {
                    if (nearbyObject.CompareTag("Recipient"))
                    {
                        haveSlicedCarot = false;
                        GoodSoup.Add(Carot);
                        counterIngredient++;
                    }
                }
                else
                {
                    haveSlicedCarot = false;
                    Vector2 spawnPosition = new Vector2(transform.position.x + dernierMouvement.x * (rayonDetection * -1), transform.position.y + dernierMouvement.y * (rayonDetection * -1));
                    Instantiate(SlicedCarot, spawnPosition, Quaternion.identity);
                }
            }
            else
            {
                //Collider2D nearbyObject = Physics2D.OverlapCircle(transform.position + (Vector3)dernierMouvement * rayonDetection, rayonDetection, objetRamassableLayer);

                if (nearbyObject)
                {
                    if (nearbyObject.CompareTag("TomaCont"))
                    {
                        haveTomato = true;
                    }
                    if (nearbyObject.CompareTag("Tomato"))
                    {
                        Destroy(nearbyObject.gameObject);
                        haveTomato = true;
                    }
                    if (nearbyObject.CompareTag("SlicedTomato"))
                    {
                        Destroy(nearbyObject.gameObject);
                        haveSlicedTomato = true;
                    }
                    if (nearbyObject.CompareTag("SoupTomato"))
                    {
                        haveTomatoSoup = true;
                        //inventory.text = "Inventory : TomatoSoup";
                        Destroy(nearbyObject);
                        Vector2 spawnPos = new Vector2(-3.5f, 1.5f);
                        Instantiate(Four, spawnPos, Quaternion.identity);
                    }


                    if (nearbyObject.CompareTag("ChampiCont"))
                    {
                        haveChampi = true;
                    }
                    if (nearbyObject.CompareTag("Champi"))
                    {
                        Destroy(nearbyObject.gameObject);
                        haveChampi = true;
                    }
                    if (nearbyObject.CompareTag("SlicedChampi"))
                    {
                        Destroy(nearbyObject.gameObject);
                        haveSlicedChampi = true;
                    }
                    if (nearbyObject.CompareTag("SoupChampi"))
                    {
                        haveChampiSoup = true;
                        //inventory.text = "Inventory : MushroomSoup";
                        Destroy(nearbyObject);
                        Vector2 spawnPos = new Vector2(-3.5f, 1.5f);
                        Instantiate(Four, spawnPos, Quaternion.identity);
                    }


                    if (nearbyObject.CompareTag("CarotCont"))
                    {
                        haveCarot = true;
                    }
                    if (nearbyObject.CompareTag("Carot"))
                    {
                        Destroy(nearbyObject.gameObject);
                        haveCarot = true;
                    }
                    if (nearbyObject.CompareTag("SlicedCarot"))
                    {
                        Destroy(nearbyObject.gameObject);
                        haveSlicedCarot = true;
                    }
                    if (nearbyObject.CompareTag("SoupCarot"))
                    {
                        haveCarotSoup = true;
                        //inventory.text = "Inventory : CarotSoup";
                        Destroy(nearbyObject);
                        Vector2 spawnPos = new Vector2(-3.5f, 1.5f);
                        Instantiate(Four, spawnPos, Quaternion.identity);
                    }

                    if(nearbyObject.CompareTag("SellMachine"))
                    {
                        isSelled = true;
                    }
                }
            }
        }
    }

    void DetecterDernierMouvement()
    {
        // Ajoute ici la logique pour d�tecter le dernier mouvement du joueur
        // Par exemple, en utilisant les axes horizontaux et verticaux d'un input.
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        // Si le joueur se d�place principalement horizontalement
        if (Mathf.Abs(horizontal) > Mathf.Abs(vertical))
        {
            dernierMouvement = new Vector2(Mathf.Sign(horizontal), 0f);
        }
        // Si le joueur se d�place principalement verticalement
        else
        {
            dernierMouvement = new Vector2(0f, Mathf.Sign(vertical));
        }
    }

    void OnDrawGizmosSelected()
    {
        // Affiche le rayon de d�tection dans l'�diteur Unity
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position + (Vector3)dernierMouvement * rayonDetection, rayonDetection);
    }
}
