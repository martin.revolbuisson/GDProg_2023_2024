using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SellSoup : MonoBehaviour
{
    public GameObject player;
    private int coinCounter;
    private int elementList;
    public Text text_coin;
    public Text text_mission;
    public Text coins_text;
    public List<GameObject> newGoodSoup = new List<GameObject>();
    public List<string> ListSoup = new List<string>();
    private bool isFinished = false;
    private string mission;

    private void Start()
    {
        ListSoup.Add("Tomate"); ListSoup.Add("Champi"); ListSoup.Add("Carot");
        coinCounter = 0;
        createNewMission();
        player = GameObject.Find("Player");
    }
    private void Update()
    {
        coins_text.text = "You gain " + coinCounter + " coins!";
        if (isFinished)
        {
            createNewMission();
        }
        text_coin.text = "Coins : " + coinCounter.ToString();
        if (player.GetComponent<RamassageObjet>().isSelled) 
        { 
            if ((player.GetComponent<RamassageObjet>().haveCarotSoup && mission == "CarotSoup") || (player.GetComponent<RamassageObjet>().haveTomatoSoup && mission == "TomatoSoup") || (player.GetComponent<RamassageObjet>().haveChampiSoup && mission == "MushroomSoup"))
            {
                System.Random rand = new System.Random();
                coinCounter += rand.Next(15, 25);
                player.GetComponent<RamassageObjet>().haveCarotSoup = false;
                player.GetComponent<RamassageObjet>().haveTomatoSoup = false;
                player.GetComponent<RamassageObjet>().haveChampiSoup = false;
                isFinished = true;
            } else
            {
                System.Random rand = new System.Random();
                coinCounter -= rand.Next(10, 17);
                player.GetComponent<RamassageObjet>().haveCarotSoup = false;
                player.GetComponent<RamassageObjet>().haveTomatoSoup = false;
                player.GetComponent<RamassageObjet>().haveChampiSoup = false;
            }
            player.GetComponent<RamassageObjet>().isSelled = false;
        }
    }



    private void createNewMission()
    {
        System.Random rand = new System.Random();
        elementList = rand.Next(0, 2);
        if (elementList ==  0)
        {
            mission = "TomatoSoup";
        } else if (elementList == 1)
        {
            mission = "MushroomSoup";
        } else if (elementList == 2)
        {
            mission = "CarotSoup";
        }
        text_mission.text = "Goal :" + mission;
        isFinished = false;
    }
}
