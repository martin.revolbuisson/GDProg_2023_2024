using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed;

    public Rigidbody2D rb;

    public Text text_time;

    public float timeLeft;

    Vector2 movement;

    private Animator animator;

    public GameObject pauseMenuUI;

    void Awake()
    {
        animator = GetComponent<Animator>();
        timeLeft = 180f;
    }

    private void Start()
    {
        pauseMenuUI.SetActive(false);
        if (Time.timeScale == 0f)
        {
            Time.timeScale = 1f;
        }
    }

    void Update()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        if (movement != Vector2.zero)
        {
            animator.SetFloat("MoveX", movement.x);
            animator.SetFloat("MoveY", movement.y);
        }

        if (timeLeft >  0)
        {
            timeLeft -= Time.deltaTime;
            updateTimer(timeLeft);
        } else
        {
            PauseGame();
        }
    }

    void FixedUpdate()
    {
        
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);
        
    }

    void updateTimer(float time)
    {
        time += 1;

        float minutes = Mathf.FloorToInt(time / 60);
        float seconds = Mathf.FloorToInt(time % 60);

        text_time.text = string.Format("Time left : {0:00} : {1:00}", minutes, seconds);
    }

    void PauseGame()
    {
        if (pauseMenuUI != null) { pauseMenuUI.SetActive(true); }
        Time.timeScale = 0f;
    }

    public void RestartGame()
    {
        if (pauseMenuUI != null) { pauseMenuUI.SetActive(false); }
        Time.timeScale = 1f;
        SceneManager.LoadScene("GameScene");
    }

    public void HomeGame()
    {
        SceneManager.LoadScene("HomeScene");
    }
}
