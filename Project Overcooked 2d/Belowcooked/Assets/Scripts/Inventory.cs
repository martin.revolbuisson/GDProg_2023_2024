using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    public Text inventory;
    private GameObject player;

    private void Start()
    {
        player = GameObject.Find("Player");
    }
    void Update()
    {
        if (player.GetComponent<RamassageObjet>().haveTomato)
        {
            inventory.text = "Inventory : Tomato";
        } else if (player.GetComponent<RamassageObjet>().haveSlicedTomato)
        {
            inventory.text = "Inventory : SlicedTomato";
        } else if (player.GetComponent<RamassageObjet>().haveTomatoSoup)
        {
            inventory.text = "Inventory : TomatoSoup";
        } else if (player.GetComponent<RamassageObjet>().haveChampi)
        {
            inventory.text = "Inventory : Mushroom";
        } else if (player.GetComponent<RamassageObjet>().haveSlicedChampi)
        {
            inventory.text = "Inventory : SlicedMushroom";
        } else if (player.GetComponent<RamassageObjet>().haveChampiSoup)
        {
            inventory.text = "Inventory : MushroomSoup";
        } else if (player.GetComponent<RamassageObjet>().haveCarot)
        {
            inventory.text = "Inventory : Carot";
        } else if (player.GetComponent<RamassageObjet>().haveSlicedCarot)
        {
            inventory.text = "Inventory : SlicedCarot";
        } else if (player.GetComponent<RamassageObjet>().haveCarotSoup)
        {
            inventory.text = "Inventory : CarotSoup";
        } else
        {
            inventory.text = "Inventory :";
        }
    }
}
