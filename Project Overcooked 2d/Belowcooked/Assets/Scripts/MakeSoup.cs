using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeSoup : MonoBehaviour
{
    public GameObject go;

    public GameObject Tomato;
    public GameObject Champi;
    public GameObject Carot;

    public GameObject FourTomato;
    public GameObject FourChampi;
    public GameObject FourCarot;

    private string TomatoSoup;
    private string ChampiSoup;
    private string CarotSoup;

    public string soupString;
    public List<GameObject> SoupTest = new List<GameObject>();

    public List<GameObject> newGoodSoup = new List<GameObject>();

    private void Start()
    {
        TomatoSoup = "Voici ce que j'obtiens avec mon code : " + Tomato.name + Tomato.name + Tomato.name;
        ChampiSoup = "Voici ce que j'obtiens avec mon code : " + Champi.name + Champi.name + Champi.name;
        CarotSoup = "Voici ce que j'obtiens avec mon code : " + Carot.name + Carot.name + Carot.name;
        soupString = "Voici ce que j'obtiens avec mon code : ";
        go = GameObject.Find("Player");
    }



    private void Update()
    {

        SoupTest = go.GetComponent<RamassageObjet>().GoodSoup;
        soupString = "Voici ce que j'obtiens avec mon code : ";

        List<GameObject> list = go.GetComponent<RamassageObjet>().GoodSoup;

        for (int i = 0; i < list.Count; i++)
        {
            soupString += list[i].name;
        }

        print(soupString);

        if (soupString == TomatoSoup)
        {
            Destroy(gameObject);
            Vector2 spawnPos = new Vector2(-3.5f, 1.5f);
            Instantiate(FourTomato, spawnPos, Quaternion.identity);
            go.GetComponent<RamassageObjet>().GoodSoup = newGoodSoup;
            list = newGoodSoup;
        }
        if (soupString == ChampiSoup)
        {
            Destroy(gameObject);
            Vector2 spawnPos = new Vector2(-3.5f, 1.5f);
            Instantiate(FourChampi, spawnPos, Quaternion.identity);
            go.GetComponent<RamassageObjet>().GoodSoup = newGoodSoup; 
            list = newGoodSoup;
        }
        if (soupString == CarotSoup)
        {
            Destroy(gameObject);
            Vector2 spawnPos = new Vector2(-3.5f, 1.5f);
            Instantiate(FourCarot, spawnPos, Quaternion.identity);
            go.GetComponent<RamassageObjet>().GoodSoup = newGoodSoup;
            list = newGoodSoup;
        }
    }
}
