# Runner Krokodil - Endless Runner Game

## Introduction:

Runner Krokodil is a solo endless runner game where you take on the role of a little crocodile, dodging obstacles to go as far as possible. Test your reflexes and see how long you can keep the crocodile running in this thrilling adventure.

## How to Launch the Game:

1. **Clone the Project:** Use the following command in your terminal to clone the project.
```bash
git clone https://gitlab.com/martin.revolbuisson/GDProg_2023_2024/-/tree/main/Runner?ref_type=heads
```
2. **Open the Project in Unity:** Launch Unity and open the project by selecting the cloned folder.
3. **Start the Game:** Navigate to the main scene and press the "Play" button to begin playing.