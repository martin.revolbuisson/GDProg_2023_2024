using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpV2 : MonoBehaviour
{
    public float jumpForce = 400f;
    public float doubleJumpForce = 300f;
    private Rigidbody2D rb;
    private bool isDown;
    private bool canDoubleJump = true;
    public Animator animator;

    AudioManager audioManager;

    public enum limits
    {
        noLimit = 0,
        limit30 = 30,
        limit60 = 60,
        limit120 = 120,
        limit240 = 240,
    }

    public limits limit;

    private void Awake()
    {
        audioManager = GameObject.FindGameObjectWithTag("Audio").GetComponent<AudioManager>();
        Application.targetFrameRate = (int)limit;
    }

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (rb.position.y <= 3.125)
        {
            animator.SetFloat("Hight", 0f);
            isDown = true;
            canDoubleJump = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Platform"))
        {
            isDown = true;
            canDoubleJump = true;
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Platform"))
        {
            if (isDown && Input.GetButtonDown("Jump"))
            {
                audioManager.PlaySFX(audioManager.jumpClip);
                animator.SetFloat("Hight", 1f);
                rb.velocity = new Vector2(rb.velocity.x, 0f);
                rb.AddForce(new Vector2(rb.velocity.x, jumpForce));
                isDown = false;
                canDoubleJump = true;
            }
            else if (canDoubleJump && Input.GetButtonDown("Jump"))
            {
                audioManager.PlaySFX(audioManager.jumpClip);
                animator.SetFloat("Hight", 1f);
                rb.velocity = new Vector2(rb.velocity.x, 0f);
                rb.AddForce(new Vector2(rb.velocity.x, doubleJumpForce));
                canDoubleJump = false;
            }
        }
    }
}
