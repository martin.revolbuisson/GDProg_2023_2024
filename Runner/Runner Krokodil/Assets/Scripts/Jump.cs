using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{
    public float jump = 10f;
    private Rigidbody2D rb;
    private bool isDown;
    public Animator animator;

    public enum limits
    {
        noLimit = 0,
        limit30 = 30,
        limit60 = 60,
        limit120 = 120,
        limit240 = 240,
    }

    public limits limit;

    private void Awake()
    {
        Application.targetFrameRate = (int)limit;
    }
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (rb.position.y <= 3.125)
        {

            animator.SetFloat("Hight", 0f);
            isDown = true;
        }
        if (isDown)
        {
            if (Input.GetButton("Jump"))
            {
                animator.SetFloat("Hight", 1f);
                rb.AddForce(new Vector2(rb.velocity.x, jump));
                isDown = false;
            }
        }
    }
}
