using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [Header("---------- Audio Source ----------")]
    [SerializeField] AudioSource music;
    [SerializeField] AudioSource jumpIt;

    [Header("---------- Audio Clip ----------")]
    public AudioClip musicClip;
    public AudioClip jumpClip;

    private void Start()
    {
        music.clip = musicClip;
        music.Play();
    }

    public void PlaySFX(AudioClip clip)
    {
        jumpIt.PlayOneShot(clip);
    }
}
