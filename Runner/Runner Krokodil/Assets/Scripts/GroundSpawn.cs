using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundSpawn : MonoBehaviour
{
    public GameObject spawnObject;
    public float spawnRate = 10f;

    private float nextSpawnTime = 0f;

    private void Update()
    {
        if (Time.time >= nextSpawnTime)
        {
            SpawnGround();
            nextSpawnTime = Time.time + 1f / spawnRate;
        }
    }

    void SpawnGround()
    {
        Vector2 spawnPosition = new Vector2(9f, 0f);

        Instantiate(spawnObject, spawnPosition, Quaternion.identity);
    }
}
