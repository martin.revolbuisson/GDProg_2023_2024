using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudSpann : MonoBehaviour
{
    public GameObject spawnObject;
    public float spawnRate = 2f;
    public float spawnRateGrowth = 0.1f;

    private float nextSpawnTime = 0f;

    private void Update()
    {
        if (Time.time >= nextSpawnTime)
        {
            SpawnCloud();
            nextSpawnTime = Time.time + 1f / spawnRate;

            // Augmente le spawnRate
            spawnRate += spawnRateGrowth * Time.deltaTime;
        }
    }

    void SpawnCloud()
    {
        float randomY = Random.Range(3.1f, 5.2f);
        Vector2 spawnPosition = new Vector2(9f, randomY);

        Instantiate(spawnObject, spawnPosition, Quaternion.identity);
    }
}

