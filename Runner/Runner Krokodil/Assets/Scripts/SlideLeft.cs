using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideLeft : MonoBehaviour
{
    public float speed = 100f;
    private float Move = -1f;
    private Rigidbody2D rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    void Update()
    {
        rb.velocity = new Vector2(Move * speed, rb.velocity.y);
        if (transform.position.x < -9)
        {
            Destroy(gameObject);
        }
    }
}
