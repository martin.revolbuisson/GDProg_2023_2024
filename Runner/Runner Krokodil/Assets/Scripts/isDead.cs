using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class isDead : MonoBehaviour
{
    public Animator animator;
    public GameObject pauseMenuUI;

    private bool gamePaused = false;

    private void Start()
    {
        ResumeGame();
    }

    void Update()
    {
        if (transform.position.x < -6.2 && !gamePaused)
        {
            animator.SetBool("isAlive", false);
            PauseGame();
        }
    }

    public void RestartGame()
    {
        ResumeGame();
        SceneManager.LoadScene("GameScene");
    }

    void PauseGame()
    {
        if (pauseMenuUI != null) { pauseMenuUI.SetActive(true); }
        Time.timeScale = 0f;
        gamePaused = true;
    }

    public void ResumeGame()
    {
        if (pauseMenuUI != null) { pauseMenuUI.SetActive(false); }
        Time.timeScale = 1.0f;
        gamePaused = false;
    }

    public void QuitGame()
    {
        ResumeGame();
        SceneManager.LoadScene("Menu");
    }
}
