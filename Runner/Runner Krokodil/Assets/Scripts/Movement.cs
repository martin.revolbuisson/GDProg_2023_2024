using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float speed = 1f;
    private float Move = 1f;
    private Rigidbody2D rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    void Update()
    {
        if (transform.position.x < -2.3f && transform.position.y <= 3.125)
        {
            speed = 1f;
        } else if (transform.position.x >= -2.3f)
        {
            speed = 0f;
        }
        rb.velocity = new Vector2(Move * speed, rb.velocity.y);
    }
}
