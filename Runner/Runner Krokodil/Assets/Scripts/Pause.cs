using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    public GameObject pauseMenuUI;

    private bool isPaused = false;

    private void Start()
    {
        pauseMenuUI.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (isPaused)
                ResumeGame();
            else
                PauseGame();
        }
    }

    public void ResumeGame()
    {
        if (pauseMenuUI != null) { pauseMenuUI.SetActive(false); }
        Time.timeScale = 1f;
        isPaused = false;
    }

    void PauseGame()
    {
        if (pauseMenuUI != null) { pauseMenuUI.SetActive(true); }
        Time.timeScale = 0f;
        isPaused = true;
    }

    public void QuiteGame()
    {
        Application.Quit();
    }
}
