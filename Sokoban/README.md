# Sokoban - 2D Puzzle Game

## Introduction:

Sokoban is a solo 2D puzzle game where you navigate through a warehouse, pushing crates to designated locations. Challenge your spatial reasoning and problem-solving skills to complete each level.

## How to Launch the Game:

1. **Clone the Project:** Use the following command in your terminal to clone the project.
```bash
git clone https://gitlab.com/martin.revolbuisson/GDProg_2023_2024/-/tree/main/Sokoban?ref_type=heads
```
2. **Open the Project in Unity:** Launch Unity and open the project by selecting the cloned folder.
3. **Start the Game:** Navigate to the main scene and press the "Play" button to begin playing.