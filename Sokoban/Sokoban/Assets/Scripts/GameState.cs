using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[Serializable]
public class GameState : MonoBehaviour
{
    #region dataToSave
    public Vector3 playerPos;
    public Sprite playerSprite;
    public List<Vector3> boxPositions;
    #endregion

    public static GameState GetCurrentState()
    {
        GameState gameStateToSave = new GameState();
        SavedElements[] elementsToSaveOnScene = GameObject.FindObjectsOfType<SavedElements>();
        gameStateToSave.boxPositions = new List<Vector3>();
        foreach (SavedElements element in elementsToSaveOnScene)
        {
            if (element.type == SavedElements.Type.Player)
            {
                gameStateToSave.playerPos = element.transform.position;
                gameStateToSave.playerSprite = element.transform.GetComponent<SpriteRenderer>().sprite;
            }
            else if (element.type == SavedElements.Type.Box)
            {
                gameStateToSave.boxPositions.Add(element.transform.position);
            }
        }
        return gameStateToSave;
    }
    public void LoadGameState()
    {
        SavedElements[] elementsToLoadOnscene = GameObject.FindObjectsOfType<SavedElements>();
        List<Vector3> remainingBoxPosition = new List<Vector3>(boxPositions);
        foreach (SavedElements elementToLoad in elementsToLoadOnscene)
        {
            if (elementToLoad.type == SavedElements.Type.Player)
            {
                elementToLoad.transform.position = playerPos;
                elementToLoad.GetComponent<SpriteRenderer>().sprite = playerSprite;
                elementToLoad.GetComponent<PlayerMovements>().UndoSpriteIndex();
            }
            else if (elementToLoad.type == SavedElements.Type.Box)
            {
                elementToLoad.transform.position = remainingBoxPosition[0];
                remainingBoxPosition.RemoveAt(0);
            }
        }
    }
}