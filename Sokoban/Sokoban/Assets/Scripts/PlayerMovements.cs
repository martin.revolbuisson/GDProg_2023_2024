using System.Collections.Generic;
using UnityEngine;

public class PlayerMovements : MonoBehaviour
{
    public GameObject player;
    private Vector3 previousPosition;
    private Vector3 nextPosition;
    public GameObject barrel;

    void Start()
    {
        previousPosition = player.transform.position;
        nextPosition = player.transform.position;
    }

    void Update()
    {
        if (Input.GetKeyDown("w"))
        {
            Move(Vector3.up);
        }
        else if (Input.GetKeyDown("s"))
        {
            Move(Vector3.down);
        }
        else if (Input.GetKeyDown("a"))
        {
            Move(Vector3.left);
        }
        else if (Input.GetKeyDown("d"))
        {
            Move(Vector3.right);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("NotMoving"))
        {
            player.transform.position = previousPosition;
        }
        else if (other.CompareTag("Deplacable"))
        {
            TryMoveObject(other.gameObject, nextPosition);
        }
    }

    void TryMoveObject(GameObject objToMove, Vector3 targetPosition)
    {
        Collider2D[] colliders = Physics2D.OverlapPointAll(targetPosition);

        if (colliders.Length == 0)
        {
            //objToMove.GetComponent<UndoManager>().AddToHistory(objToMove.transform.position);
            objToMove.transform.position = targetPosition;
        }
        else
        {
            bool isNotMoving = true;

            foreach (var collider in colliders)
            {
                if (!collider.CompareTag("NotMoving") && !collider.CompareTag("Deplacable"))
                {
                    isNotMoving = false;
                    break;
                }
            }

            if (isNotMoving)
            {
                player.transform.position = previousPosition;
            }
            else
            {
                //objToMove.GetComponent<UndoManager>().AddToHistory(objToMove.transform.position);
                objToMove.transform.position = targetPosition;
            }
        }
    }

    void Move(Vector3 direction)
    {
        GameManager.SaveGameState();

        //player.GetComponent<UndoManager>().AddToHistory(player.transform.position);
        //barrel.GetComponent<UndoManager>().AddToHistory(barrel.transform.position);

        nextPosition = player.transform.position;
        previousPosition = player.transform.position;
        player.transform.position += direction;
        nextPosition += direction * 2;
    }

    public void UndoSpriteIndex()
    {
        /*spriteIndex -= 1;
        if (spriteIndex < 0)
        {
            spriteIndex = rightSprites.Length - 1;
        }*/
    }
}
