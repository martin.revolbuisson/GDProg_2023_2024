using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TheEnd : MonoBehaviour
{
    public GameObject pauseMenuUI;
    public GameObject go;

    private bool gamePaused = false;

    private void Start()
    {
        ResumeGame();
    }

    void Update()
    {
        print(go.GetComponent<CounterEnd>().counter);
        if (go.GetComponent<CounterEnd>().counter > 4)
        {
            PauseGame();
        }
    }

    public void RestartGame()
    {
        ResumeGame();
        SceneManager.LoadScene("GameScene");
    }

    void PauseGame()
    {
        if (pauseMenuUI != null) { pauseMenuUI.SetActive(true); }
        Time.timeScale = 0f;
        gamePaused = true;
    }

    public void ResumeGame()
    {
        if (pauseMenuUI != null) { pauseMenuUI.SetActive(false); }
        Time.timeScale = 1.0f;
        gamePaused = false;
    }

    public void QuitGame()
    {
        ResumeGame();
        SceneManager.LoadScene("Menu");
    }
}