using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.Experimental.AssetDatabaseExperimental.AssetDatabaseCounters;

public class Verif : MonoBehaviour
{
    //private bool isValid = false;
    public GameObject point;
    public GameObject barrel;
    public GameObject go;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Deplacable"))
        {
            go.GetComponent<CounterEnd>().counter += 1;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Deplacable"))
        {
            go.GetComponent<CounterEnd>().counter -= 1;
        }
    }
}
