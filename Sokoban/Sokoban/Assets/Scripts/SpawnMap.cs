using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnMap : MonoBehaviour
{
    public GameObject PlayerCharacter;
    public GameObject Point;
    public GameObject Wall;
    public GameObject Barrel;

    private void Start()
    {
        SpawnObject();
    }

    void SpawnObject()
    {
        GameObject players = Instantiate(PlayerCharacter);
        players.transform.position = new Vector3(1, 1, 0);
        for (int i = -5; i < 6; i++)
        {
            GameObject wall = Instantiate(Wall);
            wall.transform.position = new Vector3(i, 5, 0);
        }
        for (int i = -5; i < 6; i++)
        {
            GameObject wall = Instantiate(Wall);
            wall.transform.position = new Vector3(i, -5, 0);
        }
        for (int i = -4; i < 5; i++)
        {
            GameObject wall = Instantiate(Wall);
            wall.transform.position = new Vector3(-5, i, 0);
        }
        for (int i = -4; i < 5; i++)
        {
            GameObject wall = Instantiate(Wall);
            wall.transform.position = new Vector3(5, i, 0);
        }
        for (int i = -4; i < 5; i++)
        {
            GameObject wall = Instantiate(Wall);
            wall.transform.position = new Vector3(i, -4, 0);
        }
        for (int i = -4; i < 5; i++)
        {
            GameObject wall = Instantiate(Wall);
            wall.transform.position = new Vector3(i, 4, 0);
        }
        for (int i = -4; i < -3; i++)
        {
            GameObject wall = Instantiate(Wall);
            wall.transform.position = new Vector3(i, 3, 0);
        }
        for (int i = -4; i < 0; i++)
        {
            GameObject wall = Instantiate(Wall);
            wall.transform.position = new Vector3(i, 2, 0);
        }
        for (int i = 0; i < 4; i++)
        {
            GameObject wall = Instantiate(Wall);
            wall.transform.position = new Vector3(4, i, 0);
        }
        for (int i = 0; i < 3; i++)
        {
            GameObject wall = Instantiate(Wall);
            wall.transform.position = new Vector3(3, i, 0);
        }
        for (int i = 2; i < 3; i++)
        {
            GameObject wall = Instantiate(Wall);
            wall.transform.position = new Vector3(2, i, 0);
        }
        for (int i = -4; i < -1; i++)
        {
            GameObject wall = Instantiate(Wall);
            wall.transform.position = new Vector3(i, -1, 0);
        }
        for (int i = -4; i < -1; i++)
        {
            GameObject wall = Instantiate(Wall);
            wall.transform.position = new Vector3(i, -2, 0);
        }
        for (int i = -4; i < 0; i++)
        {
            GameObject wall = Instantiate(Wall);
            wall.transform.position = new Vector3(i, -3, 0);
        }
        for (int i = 2; i < 5; i++)
        {
            GameObject wall = Instantiate(Wall);
            wall.transform.position = new Vector3(i, -3, 0);
        }
        GameObject barrel1 = Instantiate(Barrel);
        GameObject barrel2 = Instantiate(Barrel);
        GameObject barrel3 = Instantiate(Barrel);
        GameObject barrel4 = Instantiate(Barrel);
        GameObject barrel5 = Instantiate(Barrel);
        barrel1.transform.position = new Vector3(0, 1, 0);
        barrel2.transform.position = new Vector3(0, -2, 0);
        barrel3.transform.position = new Vector3(3, -2, 0);
        barrel4.transform.position = new Vector3(-1, 0, 0);
        barrel5.transform.position = new Vector3(-3, 0, 0);

        GameObject point1 = Instantiate(Point);
        GameObject point2 = Instantiate(Point);
        GameObject point3 = Instantiate(Point);
        GameObject point4 = Instantiate(Point);
        GameObject point5 = Instantiate(Point);
        point1.transform.position = new Vector3(-3, 3, 0);
        point2.transform.position = new Vector3(3, 3, 0);
        point3.transform.position = new Vector3(2, 1, 0);
        point4.transform.position = new Vector3(-3, 1, 0);
        point5.transform.position = new Vector3(0, -1, 0);

    }
}
