using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UndoManager : MonoBehaviour
{
    private Stack<Vector3> positionsHistory = new Stack<Vector3>();

    void Start()
    {
        // Ajoute la position initiale au d�but
        AddToHistory(transform.position);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.U))
        {
            UndoMove();
        }
    }

    public void AddToHistory(Vector3 position)
    {
        positionsHistory.Push(position);
    }

    void UndoMove()
    {
        if (positionsHistory.Count > 1)
        {
            // Retire la position actuelle de l'historique
            positionsHistory.Pop();

            // R�cup�re la position pr�c�dente
            Vector3 previousPosition = positionsHistory.Peek();

            // D�place l'objet � la position pr�c�dente
            transform.position = previousPosition;
        }
    }
}
